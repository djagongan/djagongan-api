import { config } from 'dotenv';
import { IEnv } from '../models';
config();

const env: IEnv = {
  PORT: 4000,
  JWT_SECRET: '',
  JWT_EXPIRY: 300,
  MONGO_HOST: '',
  MONGO_PORT: '',
  MONGO_USER: '',
  MONGO_PASSWORD: '',
  MONGO_DB: ''
};

export const initEnv = () => {
  const { PORT, JWT_SECRET, JWT_EXPIRY, MONGO_HOST, MONGO_PORT, MONGO_USER, MONGO_PASSWORD, MONGO_DB } = process.env;

  env.PORT = parseInt(PORT ?? '4000');
  env.JWT_SECRET = JWT_SECRET ?? '';
  env.JWT_EXPIRY = parseInt(JWT_EXPIRY ?? '300');
  env.MONGO_HOST = MONGO_HOST ?? '';
  env.MONGO_PORT = MONGO_PORT ?? '';
  env.MONGO_USER = MONGO_USER ?? '';
  env.MONGO_PASSWORD = MONGO_PASSWORD ?? '';
  env.MONGO_DB = MONGO_DB ?? '';
};

export const getEnv = (): IEnv => env;
