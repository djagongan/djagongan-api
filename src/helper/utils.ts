import { decodeJwt } from 'jose';
import { Context } from 'koa';
import { Types } from 'mongoose';
import { Socket } from 'socket.io';

export const decodeJwtFromCtx = <T>(ctx: Context) => {
  return decodeJwt(ctx.request.headers.authorization?.split(' ')[1] ?? '') as T;
};

export const decodeJwtFromSocket = <T>(socket: Socket) => {
  const { token } = socket.handshake.auth
  return decodeJwt(token) as T;
};

export const objectIdNew = (): Types.ObjectId => new Types.ObjectId()

export const objectIdFromHex = (hex: string): Types.ObjectId => new Types.ObjectId(hex)