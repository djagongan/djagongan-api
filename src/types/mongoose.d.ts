import { Model } from "mongoose";

type TMongooseModel = Model<any, {}, {}, {}, any, any>
type TMongooseModels = Readonly<{
  User: TMongooseModel;
  Room: TMongooseModel;
}>

declare module 'mongoose' {
  interface Connection {
    models: TMongooseModels
  }
}
