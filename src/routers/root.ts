import Router from '@koa/router';
import { respOk } from '../helper';

export const root = new Router();

root.get('/', (ctx, next) => {
  ctx.body = respOk(null, 'Running');
});
