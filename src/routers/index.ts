import Koa from 'koa';
import { auth } from './auth';
import { root } from './root';

export const routers = (app: Koa) => {
  [root, auth].forEach(r => {
    app.use(r.routes()).use(r.allowedMethods());
  });
};
