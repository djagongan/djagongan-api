import { IPayloadJwt, IPayloadLogin, IPayloadRegister } from '@/models'
import { IUser } from '@/models/database'
import Router from '@koa/router'
import { SignJWT } from 'jose'
import { TextEncoder } from 'util'
import { decodeJwtFromCtx, getEnv, respOk } from '../helper'
import { jwt } from '../middlewares'
import { authService } from '../services'

export const auth = new Router({ prefix: '/auth' })

auth.post('/login', async (ctx) => {
  const payload = ctx.request.body as IPayloadLogin

  const { JWT_EXPIRY, JWT_SECRET } = getEnv()
  const secret = new TextEncoder().encode(JWT_SECRET)
  const expiry = `${JWT_EXPIRY}s`

  if (!JWT_SECRET) {
    throw new Error('jwt not setup properly')
  }

  const user = await authService.login(payload)

  const dataAccessToken = {
    userId: user?._id,
    type: 'accessToken'
  }

  const dataRefreshToken = {
    userId: payload.username,
    type: 'refreshToken'
  }

  const [accessToken, refreshToken] = await Promise.all([
    new SignJWT(dataAccessToken)
      .setProtectedHeader({ alg: 'HS256' })
      .setIssuedAt()
      .setExpirationTime(expiry)
      .sign(secret),
    new SignJWT(dataRefreshToken)
      .setProtectedHeader({ alg: 'HS256' })
      .setIssuedAt()
      .setExpirationTime(expiry)
      .sign(secret)
  ])

  ctx.body = respOk({
    accessToken,
    refreshToken
  })
})

auth.get('/profile', jwt(), async (ctx) => {
  const payload = decodeJwtFromCtx<IPayloadJwt>(ctx)

  const res = await authService.profile(payload.userId)

  delete (res as any)?.password

  ctx.body = respOk(res)
})

auth.post('/register', async (ctx) => {
  const payload = ctx.request.body as IPayloadRegister

  const res = await authService.register(<IUser>{
    ...payload
  })

  ctx.body = respOk(res)
})
