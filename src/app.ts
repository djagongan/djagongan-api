import cors from '@koa/cors';
import { createServer } from 'http';
import Koa from 'koa';
import koaBody from 'koa-body';
import helmet from 'koa-helmet';
import { Server } from 'socket.io';
import { getEnv, initEnv, print } from './helper';
import { logger } from './middlewares';
import { errorHandler } from './middlewares/error';
import { routers } from './routers';
import Socket from './sockets';

const app = new Koa();

initEnv();

const { PORT } = getEnv();

app.use(
  cors({
    origin: 'http://localhost:3000'
  })
);
app.use(logger());
app.use(helmet());
app.use(koaBody());
app.use(errorHandler());

routers(app);

const httpServer = createServer(app.callback());
const io = new Server(httpServer, {
  cors: {
    origin: ['http://localhost:3000', 'http://127.0.0.1:3000']
    // credentials: true,
  }
});
const socket = new Socket(io);

print.info('\n', 'Server live on port:', PORT, '\n');

httpServer.listen(PORT);
