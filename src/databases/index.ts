import { Connection } from 'mongoose';
import { mongoConnection } from './mongo';
import { schemas } from './schemas';


export * from './mongo';
export * from './schemas';

export const mongoClient = async (): Promise<Connection> => {
  const conn = await mongoConnection()
  conn.model('User', schemas.user, 'users')
  conn.model('Room', schemas.room, 'rooms');

  return conn
}