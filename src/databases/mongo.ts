import { Connection, createConnection } from 'mongoose';
import { getEnv } from '../helper';

export const mongoConnection = async (): Promise<Connection> => {
  const { MONGO_DB, MONGO_HOST, MONGO_PASSWORD, MONGO_PORT, MONGO_USER } = getEnv();
  const uri = `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_DB}`;
  const connection = await createConnection(uri, { authSource: 'admin' })
  return connection
};
