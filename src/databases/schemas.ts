import { IRoom, IUser } from '@/models'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import { Schema, Types, model } from 'mongoose'
dayjs.extend(utc)

export const schemas = {
  user: new Schema<IUser>({
    _id: { type: Schema.ObjectId, default: new Types.ObjectId() },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    image: { type: String },
    isActive: { type: Boolean },
    username: { type: String },
    createdAt: { type: Date, default: dayjs().utc() },
    updatedAt: { type: Date, default: null },
    deletedAt: { type: Date, default: null }
  }),
  room: new Schema<IRoom>({
    _id: { type: Schema.ObjectId, default: new Types.ObjectId() },
    name: { type: String, required: true },
    image: { type: String },
    type: { type: String, enum: ['group', 'direct'], required: true },
    isMuted: { type: Boolean },
    isPinned: { type: Boolean },
    createdAt: { type: Date, default: dayjs().utc() },
    updatedAt: { type: Date, default: null },
    deletedAt: { type: Date, default: null }
  })
}

export const User = model('User', schemas.user, 'users')
export const Room = model('Room', schemas.room, 'rooms')
