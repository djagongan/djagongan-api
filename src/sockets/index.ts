import { IPayloadJwt, IUser } from '@/models';
import { Server } from 'socket.io';
import { decodeJwtFromSocket, objectIdFromHex, print, respErr, respOk } from '../helper';
import { userRepository } from '../repositories';
import { userEvent } from './helper';
import { jwt } from './middlewares';
import { ISocketUserEvent } from './models/user';

interface SocketSession {
  userId: string
  socketIds: string[]
}

export default class Socket {
  private __server: Server;

  private __allClients: SocketSession[] = [];

  constructor(server: Server) {
    this.__server = server;

    this.__server.use(jwt);

    this.__server.on('connection', socket => {
      print.info(`connection ${socket.id} created`);

      const claims = decodeJwtFromSocket<IPayloadJwt>(socket)

      const indexClient = this.__allClients.map(e => e.userId).indexOf(claims.userId)

      if (indexClient > -1) {
        this.__allClients[indexClient].socketIds.push(socket.id)
      } else {
        this.__allClients.push({
          userId: claims.userId,
          socketIds: [socket.id]
        })
      }

      socket.on('disconnect', data => {
        print.info(`connection ${socket.id} disconnected`);

        const indexClient = this.__allClients.findIndex(e => e.socketIds.includes(socket.id));
        if (indexClient < 0) {
          return
        }
        if (this.__allClients[indexClient].socketIds.length > 1) {
          const indexSocketId = this.__allClients[indexClient].socketIds.indexOf(socket.id)
          this.__allClients[indexClient].socketIds.splice(indexClient, 1);
        } else {
          this.__allClients.splice(indexClient, 1);
        }
      });

      socket.on('chat', data => {
        console.log(`chat data`, data);

        setTimeout(() => {
          socket.emit('chat', 'from server', data);
        }, 3000);
      });

      socket.on('user', async (data: ISocketUserEvent) => {
        const claims = decodeJwtFromSocket<IPayloadJwt>(socket)
        const userId = objectIdFromHex(claims.userId)
        const target = `${data.action}:${claims.userId}`
        const socketIds = this.__allClients.find(e => e.userId === claims.userId)?.socketIds ?? []
        console.log('socketIds', claims.userId, socketIds)
        if (data.action === 'getProfile') {
          const res = await userRepository.findOneById(userId)
          if (res.error) {
            this.__server.to(socketIds).emit(target, respErr(res.error))
          } else {
            delete (res.data as any)?.password
            this.__server.to(socketIds).emit(target, respOk(res.data))
          }
        } else if (data.action === 'updateProfile') {
          const payload = data.data as { firstName: string, lastName: string, username: string }
          const res = await userRepository.findOneById(userId)
          const target = userEvent('getProfile', claims.userId)
          if (res.error) {
            this.__server.to(socketIds).emit(target, respErr(res.error))
          } else {
            const newData = <IUser>{ ...res.data, ...payload }
            const resUpdate = await userRepository.updateOne(userId, newData)
            if (resUpdate.error) {
              this.__server.to(socketIds).emit(target, respErr(res.error))
            } else {
              const resNewData = await userRepository.findOneById(userId)
              this.__server.to(socketIds).emit(target, respOk(resNewData.data))
            }
          }
        }
      })
    });
  }
}
