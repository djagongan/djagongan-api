export type TSocketUserAction = 'getProfile' | 'updateProfile' | 'changePassword' | 'changeEmail'

export interface ISocketUserEvent {
  action: TSocketUserAction
  data: any
}