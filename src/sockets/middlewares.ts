import { jwtVerify } from 'jose';
import { Socket } from 'socket.io';
import { TextEncoder } from 'util';
import { getEnv } from '../helper';
import { AppError } from '../models';

type TSocketMiddleware = (socket: Socket, next: () => void) => void;

export const jwt: TSocketMiddleware = async (socket, next) => {
  const { JWT_SECRET } = getEnv();
  const secret = new TextEncoder().encode(JWT_SECRET);
  try {
    const token = socket.handshake.auth.token;

    if (!token) {
      throw new AppError(401, 'token is missing');
    }

    const { payload } = await jwtVerify(token, secret);

    if (payload) {
      await next();
    } else {
      throw new AppError(401, 'token is invalid');
    }
  } catch (e) {
    if (e instanceof AppError) {
      console.log(e.message ?? 'token error');
    }
  }
};
