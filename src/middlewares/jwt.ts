import { jwtVerify } from 'jose';
import { Middleware } from 'koa';
import { TextEncoder } from 'util';
import { getEnv, respErr } from '../helper';

export const jwt = (): Middleware => {
  return async (c, next) => {
    const { JWT_SECRET } = getEnv();
    const secret = new TextEncoder().encode(JWT_SECRET);
    try {
      const token = c.headers.authorization?.split(' ')[1];

      if (!token) {
        throw 'token is missing';
      }

      const { payload } = await jwtVerify(token, secret);

      if (payload) {
        await next();
      } else {
        throw 'token is invalid';
      }
    } catch (e: any) {
      c.status = 401;
      c.body = respErr(e.message ?? 'token error');
    }
  };
};
