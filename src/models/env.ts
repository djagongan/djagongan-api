export interface IEnv {
  PORT: number;
  JWT_SECRET: string;
  JWT_EXPIRY: number;
  MONGO_HOST: string;
  MONGO_PORT: string;
  MONGO_USER: string;
  MONGO_PASSWORD: string;
  MONGO_DB: string;
}
