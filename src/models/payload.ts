export interface IPayloadJwt {
  userId: string
  type: string
}

export interface IPayloadLogin {
  username: string;
  password: string;
}

export interface IPayloadRegister {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}
