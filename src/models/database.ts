import { Schema } from 'mongoose';
import { IPayloadLogin } from './payload';

export type TRoomType = 'direct' | 'group';
export type TRoomUserRole = 'owner' | 'admin' | 'user';

export interface IAudit {
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
}

export interface IUser extends IPayloadLogin, IAudit {
  _id: Schema.Types.ObjectId;
  firstName: string;
  lastName: string;
  image: string;
  email: string;
  isActive: boolean;
}

export interface IRoomUser {
  _id: Schema.Types.ObjectId;
  role: TRoomUserRole;
  joinedAt: string;
  leaveAt?: string;
}

export interface IRoom extends IAudit {
  _id: Schema.Types.ObjectId;
  type: TRoomType;
  name: string;
  image: string;
  users: IRoomUser[];
  isPinned: boolean;
  isMuted: boolean;
}
