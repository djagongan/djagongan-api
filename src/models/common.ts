export interface IMap {
  [key: string]: any;
}

export class AppError extends Error {
  code: number

  constructor(code: number, message: string | undefined, options?: ErrorOptions | undefined) {
    super(message, options)
    this.code = code
  }
}