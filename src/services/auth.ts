import { IUser } from "@/models/database";
import { Schema } from "mongoose";
import { hashVerify, objectIdFromHex } from "../helper";
import { AppError, IPayloadLogin } from "../models";
import { userRepository } from "../repositories";

export default {
  register: async (data: IUser): Promise<Schema.Types.ObjectId> => {
    const res = await userRepository.insertOne(data);

    if (res.error) {
      throw new Error('register user failed. ' + res.error)
    }

    return res.data!
  },
  login: async (data: IPayloadLogin): Promise<IUser | undefined> => {
    const res = await userRepository.findOne({
      $or: [{
        username: data.username
      }, {
        email: data.username
      }]
    })

    if (res.error) {
      throw new AppError(500, 'login user failed. ' + res.error)
    }

    const user = res.data!
    const isValid = await hashVerify(data.password, user.password)

    if (!isValid) {
      throw new AppError(401, 'invalid credentials')
    }

    return user
  },
  profile: async (id: string): Promise<IUser | undefined> => {
    const res = await userRepository.findOneById(objectIdFromHex(id));

    if (res.error) {
      throw new AppError(500, 'fetching profile failed. ' + res.error)
    }

    return res.data!
  }
}