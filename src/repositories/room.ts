import { IMap } from '@/models';
import { IRoom } from '@/models/database';
import { Schema } from 'mongoose';
import { mongoClient } from '../databases';

const collectionName = 'rooms';

export const insertOne = async (data: IRoom): Promise<{ data?: Schema.Types.ObjectId; error?: any }> => {
  const conn = await mongoClient();
  try {
    const doc = new conn.models.Room(data)
    return {
      data: doc._id
    };
  } catch (error) {
    return {
      error
    };
  } finally {
    conn.close();
  }
};

export const findOne = async (filter: IMap): Promise<{ data?: IRoom; error?: any }> => {
  const conn = await mongoClient();
  try {
    const doc = await conn.models.Room.findOne(filter);
    if (!doc) {
      throw new Error('data not found')
    }
    return {
      data: <IRoom>{ ...doc }
    };
  } catch (error) {
    return { error };
  } finally {
    conn.close();
  }
};

export const findOneById = async (id: Schema.Types.ObjectId): Promise<{ data?: IRoom; error?: any }> => {
  return await findOne({ _id: id });
};

export const deleteOne = async (id: Schema.Types.ObjectId): Promise<{ data?: number; error?: any }> => {
  const conn = await mongoClient();
  try {
    const res = await conn.models.Room.deleteOne({ _id: id });
    return {
      data: res.deletedCount
    };
  } catch (error) {
    return { error };
  } finally {
    conn.close();
  }
};
