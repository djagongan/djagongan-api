import dayjs from 'dayjs';
import { Schema, Types } from 'mongoose';
import { mongoClient } from '../databases';
import { hash } from '../helper';
import { IMap, IUser } from '../models';

const collectionName = 'users';

export const insertOne = async (data: IUser): Promise<{ data?: Schema.Types.ObjectId | undefined; error?: any }> => {
  const conn = await mongoClient();
  try {
    const doc = new conn.models.User({ ...data, password: await hash(data.password) })
    const res = await doc.save();
    return {
      data: res._id
    };
  } catch (error) {
    return {
      error
    };
  } finally {
    conn.close();
  }
};

export const findOne = async (filter: IMap): Promise<{ data?: IUser; error?: any }> => {
  const conn = await mongoClient();
  try {
    const doc = await conn.models.User.findOne(filter);
    if (!doc) {
      throw new Error('data not found')
    }
    return {
      data: doc.toJSON()
    };
  } catch (error) {
    return {
      error
    };
  } finally {
    conn.close();
  }
};

export const findOneById = async (id: Types.ObjectId): Promise<{ data?: IUser; error?: any }> => {
  return await findOne({ _id: id });
};

export const updateOne = async (id: Types.ObjectId, data: IUser): Promise<{ data?: number; error?: any }> => {
  const conn = await mongoClient();
  try {
    data.updatedAt = dayjs().utc().toDate()
    const res = await conn.models.User.updateOne({ _id: id }, data);
    return {
      data: res.modifiedCount
    };
  } catch (error) {
    return {
      error
    };
  } finally {
    conn.close();
  }
};

export const deleteOne = async (id: Types.ObjectId): Promise<{ data?: number; error?: any }> => {
  const conn = await mongoClient();
  try {
    const res = await conn.models.User.deleteOne({ _id: id });
    return {
      data: res.deletedCount
    };
  } catch (error) {
    return {
      error
    };
  } finally {
    conn.close();
  }
};
